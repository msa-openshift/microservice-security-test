package com.idaltec.msa.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class DbUser extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Map<String, Object> data = new HashMap<String, Object>();

	public DbUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		// TODO Auto-generated constructor stub
	}

	public DbUser(String username, Collection<? extends GrantedAuthority> authorities) {
		super(username, null, true, true, true, true, authorities);
	}

	public DbUser(String username, Collection<? extends GrantedAuthority> authorities, Map<String, Object> extraData) {
		super(username, username, true, true, true, true, authorities);
		setData(extraData);
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DbUser [data=" + data + ", getAuthorities()=" + getAuthorities() + ", getUsername()=" + getUsername()
				+ "]";
	}

}
