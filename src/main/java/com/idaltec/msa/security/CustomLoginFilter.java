package com.idaltec.msa.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;


public class CustomLoginFilter extends OncePerRequestFilter implements Filter, InitializingBean {

	private static Logger log = LoggerFactory.getLogger(CustomLoginFilter.class);
	
		protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

			System.out.println("*************** Custom filter *************");
		    printParameters( request) ;
		    
		    String userId = "DEMO_USER";

		    //websso token
		    String user = request.getHeader("REMOTE_USER");
	        
	        // validate the value in xAuth
	        if(StringUtils.isEmpty(userId)){
	            throw new SecurityException();
	        }                            
	        
	        // Create our Authentication and let Spring know about it
	        Authentication auth = new UsernamePasswordAuthenticationToken(userId,null);
	        SecurityContextHolder.getContext().setAuthentication(auth);    
	        
		    System.out.println("*************** END Custom filter *************");
		    filterChain.doFilter(request, response);
		    
		}		

		
		private void printParameters(final HttpServletRequest request) {
			Enumeration<String> headers = request.getHeaderNames();
			while (headers.hasMoreElements()) {
				log.info("HEADER_ " + headers.nextElement());
			}
			
			
			Enumeration<String> attrs= request.getAttributeNames();
			while (attrs.hasMoreElements()) {
				log.info("attrs: " + attrs.nextElement());
			}
		}

		
		
}
