package com.idaltec.msa.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();

		System.out.println("***************DENTRO****************");
		
		// CIS
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_1"));
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_2"));
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_3"));
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_4"));

		Map<String,Object> data = new HashMap<String,Object>();
		data.put("key_1","Data_1");
		data.put("key_2","Data_2");
		data.put("key_3","Data_3");
		
		DbUser myUser = new DbUser(name, grantedAuthorities, data);
		// use the credentials and authenticate against the third-party system
		return new UsernamePasswordAuthenticationToken(myUser, null, grantedAuthorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
