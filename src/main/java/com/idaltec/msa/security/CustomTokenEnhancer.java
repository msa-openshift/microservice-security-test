package com.idaltec.msa.security;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * 
 * @author infs
 *
 */
public  class CustomTokenEnhancer extends JwtAccessTokenConverter {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken,
            OAuth2Authentication authentication) {

    	/*
        DbUser user = (DbUser) authentication.getPrincipal();
        Map<String, Object> info = new LinkedHashMap<String, Object>(
                accessToken.getAdditionalInformation());
        info.put("extraData", user.getData());

        
        
*/
    	DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
        //customAccessToken.setAdditionalInformation(info);
        
        
        OAuth2AccessToken token = super.enhance(customAccessToken, authentication); 
        
        System.out.println("*********************************************");
        System.out.println("JWT: " + token.getValue());
        System.out.println("*********************************************");
        
        return token;

    }
}